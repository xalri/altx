//! An implementation of the altx archive format.

use std::fs;
use std::io::{Cursor, Read, Write};
use std::path::*;
use std::u64;

use crate::error::*;
use byteorder::LittleEndian as LE;
use byteorder::{ReadBytesExt, WriteBytesExt};
use crc::crc32;

/// A decompressed and loaded `.altx` file.
#[derive(Clone)]
pub struct Altx {
   paths: Vec<String>,
   raw: Vec<u8>,
   compressed: Vec<u8>,
   data: Vec<(usize, usize)>,
   crc32: u32,
   size: usize,
}

impl Altx {
   /// Create a new empty archive
   pub fn new() -> Result<Altx> {
      let mut data = Altx {
         paths: Vec::new(),
         raw: Vec::new(),
         compressed: Vec::new(),
         data: Vec::new(),
         crc32: 0,
         size: 0,
      };
      data.update()?;
      Ok(data)
   }
   /// Try to read data from an input stream into an `Altx`. Decompresses the LZMA,
   /// decodes some index information, and finally stores the raw data in memory.
   pub fn from_reader<R: Read>(mut r: R) -> Result<Self> {
      let mut raw_compressed = Vec::new();
      r.read_to_end(&mut raw_compressed)?;
      Self::from_vec(raw_compressed)
   }

   /// Load an altx file from memory
   pub fn from_vec(raw_compressed: Vec<u8>) -> Result<Self> {
      let size = raw_compressed.len();
      let crc32 = crc32::checksum_ieee(&raw_compressed);

      let s = xz2::stream::Stream::new_lzma_decoder(u64::MAX)?;
      let mut decoder = xz2::read::XzDecoder::new_stream(Cursor::new(raw_compressed), s);
      let mut raw = Vec::new();
      decoder.read_to_end(&mut raw)?;
      let mut raw = Cursor::new(raw);

      let mut paths = Vec::new();
      let mut sizes = Vec::new();
      let files = raw.read_u32::<LE>()? as usize;
      for _ in 0..files {
         let string = crate::read_string(&mut raw)?;
         paths.push(string);
      }
      for _ in 0..files {
         sizes.push(raw.read_u32::<LE>()?);
      }

      let mut x = raw.position() as usize;
      let raw = raw.into_inner();

      let mut a = Altx {
         paths,
         raw,
         compressed: decoder.into_inner().into_inner(),
         data: Vec::new(),
         crc32,
         size,
      };

      for s in &sizes {
         a.data.push((x, x + *s as usize));
         x += *s as usize;
      }

      Ok(a)
   }

   /// Write the archive to a writer.
   pub fn to_writer(&self, writer: &mut dyn Write) -> Result<()> {
      Ok(writer.write_all(&self.compressed)?)
   }

   /// Get the CRC32 of the compressed archive -- calculated when the archive
   /// is loaded or updated.
   pub fn crc32(&self) -> u32 {
      self.crc32
   }

   /// Get the size in bytes of the compressed archive -- calculated when the
   /// archive is loaded or updated.
   pub fn size(&self) -> usize {
      self.size
   }

   /// Tries to read data from a file into an `Altx`. See `from_reader`.
   pub fn from_file<P: AsRef<Path>>(p: P) -> Result<Altx> {
      Self::from_reader(fs::File::open(p)?)
   }

   /// Update the archive to add new files -- recreates the compressed data
   /// and calculate the new size and crc32.
   fn update(&mut self) -> Result<()> {
      let mut data = Cursor::new(Vec::new());

      data.write_u32::<LE>(self.paths.len() as u32)?;
      for path in &self.paths {
         crate::write_string(&mut data, path)?;
      }
      for &(a, b) in &self.data {
         data.write_u32::<LE>((b - a) as u32)?;
      }

      let mut ranges = Vec::new();
      for &(a, b) in &self.data {
         let start = data.position();
         data.write_all(&self.raw[a..b])?;
         let end = data.position();
         ranges.push((start as usize, end as usize));
      }

      self.raw = data.into_inner();
      self.data = ranges;

      let mut opts = xz2::stream::LzmaOptions::new_preset(5)?;
      opts.dict_size(8388608);
      let s = xz2::stream::Stream::new_lzma_encoder(&opts)?;

      let compressed = Cursor::new(Vec::new());
      let mut encoder = xz2::write::XzEncoder::new_stream(compressed, s);
      encoder.write_all(&self.raw[..])?;

      self.compressed = encoder.finish()?.into_inner();
      self.size = self.compressed.len();
      self.crc32 = crc32::checksum_ieee(&self.compressed);
      Ok(())
   }

   /// Get the compressed archive data.
   pub fn compressed(&self) -> &[u8] {
      &self.compressed
   }

   /// Consume the archive returning the compressed data.
   pub fn into_vec(self) -> Vec<u8> {
      self.compressed
   }

   /// Iterate through the archive's files.
   pub fn files(&self) -> ArchiveFiles {
      ArchiveFiles { ar: self, index: 0 }
   }

   /// Find a file in the archive given it's path.
   pub fn find_file(&self, path: &str) -> Option<&[u8]> {
      self.paths.iter().position(|p| p == path).map(|index| {
         let slice = &self.data[index];
         &self.raw[slice.0..slice.1]
      })
   }

   /// Add a file to the archive
   pub fn add_file(&mut self, path: &str, data: &[u8]) -> Result<()> {
      self.paths.push(path.to_string());

      // Add the data to the buffer, storing the start and end positions.
      let start = self.raw.len();
      self.raw.extend(data);
      let end = self.raw.len();

      self.data.push((start, end));
      self.update()?;

      Ok(())
   }
}

/// A reference to a single file from an archive, with its name and raw data.
/// No metadata besides the filename is supplied by the .altx format.
pub struct ArchiveFile<'a> {
   pub path: &'a str,
   pub data: &'a [u8],
}

/// Borrowing iterator to access `ArchiveFile`s from an archive.
pub struct ArchiveFiles<'a> {
   ar: &'a Altx,
   index: usize,
}

impl<'a> Iterator for ArchiveFiles<'a> {
   type Item = ArchiveFile<'a>;
   fn next(&mut self) -> Option<Self::Item> {
      if self.index < self.ar.paths.len() {
         let path = &self.ar.paths[self.index];
         let portion = &self.ar.data[self.index];
         self.index += 1;
         Some(ArchiveFile {
            path: path,
            data: &self.ar.raw[portion.0..portion.1],
         })
      } else {
         None
      }
   }
}
