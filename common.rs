use crate::error::*;
use crate::xml::*;
use derivative::*;

/// The colour used for polygons with no colour specified.
pub const DEFAULT_COLOR: Colour = Colour::from_u8(100, 100, 100, 255);

/// The default colour for map backgrounds.
pub const DEFAULT_BG_COLOR: Colour = Colour::from_u8(50, 50, 50, 255);

/// The default colour for player names.
pub const DEFAULT_TEXT_COLOR: Colour = Colour::from_u8(200, 200, 200, 255);

/// A team in some game.
#[derive(PartialEq, Debug, Clone, Copy, Derivative, Fragment)]
#[derivative(Default)]
pub enum Team {
   A,
   B,
   #[derivative(Default)]
   Spectator,
   Red,
   Blue,
   Green,
   Yellow,
   Orange,
   Purple,
   Azure,
   Pink,
   Brown,
}

/// A transformation.
#[derive(Derivative, Clone, Copy, Debug, PartialEq, Fragment)]
#[derivative(Default)]
pub struct TransformComponents {
   #[xml(required, name = "translate")]
   pub position: Vec2,
   #[xml(name = "scale")]
   #[derivative(Default(value = "1.0"))]
   pub scale: f32,
   #[xml(name = "rotate")]
   pub orientation: f32,
   #[xml(name = "flipX")]
   pub flip_x: bool,
   #[xml(name = "flipY")]
   pub flip_y: bool,
}

#[derive(Clone, Copy, Debug, PartialEq, Fragment, Default)]
pub struct Vec2 {
   pub x: f32,
   pub y: f32,
}

impl Vec2 {
   pub fn new(x: f32, y: f32) -> Vec2 {
      Vec2 { x, y }
   }
}

#[allow(non_snake_case)]
#[derive(Clone, Copy, Debug, PartialEq, Fragment, Default)]
pub struct Bounds {
   pub minX: f32,
   pub minY: f32,
   pub maxX: f32,
   pub maxY: f32,
}

#[derive(Clone, Copy, Debug, PartialEq, Default)]
pub struct Dimensions {
   pub width: f32,
   pub height: f32,
}

impl Dimensions {
   pub fn to_vec(self) -> Vec2 {
      Vec2 {
         x: self.width,
         y: self.height,
      }
   }
}

impl Attribute for Dimensions {
   /// Read a dimension string (e.g. "100,300").
   fn parse_attr(s: &str) -> Result<Self> {
      let x: Vec<&str> = s.split(',').collect();
      if x.len() == 2 {
         Ok(Dimensions {
            width: x[0].parse()?,
            height: x[1].parse()?,
         })
      } else {
         bail!("invalid dimension string")
      }
   }

   /// Write the dimensions to a `String`.
   fn write_attr(&self) -> String {
      format!("{},{}", self.width, self.height)
   }
}

#[derive(Clone, Copy, Debug, PartialEq, Default)]
pub struct Colour {
   r: f32,
   g: f32,
   b: f32,
   a: f32,
}

impl Attribute for Colour {
   /// Parses an altx colour
   fn parse_attr(s: &str) -> Result<Self> {
      let c: i32 = s.parse()?;
      Ok(Colour {
         b: (c & 255) as f32 / 255.0,
         g: (c >> 8 & 255) as f32 / 255.0,
         r: (c >> 16 & 255) as f32 / 255.0,
         a: (c >> 24 & 255) as f32 / 255.0,
      })
   }

   /// Writes a colour to the altx format.
   fn write_attr(&self) -> String {
      let c: i32 = ((self.b * 255.0) as i32) << 0
         | ((self.g * 255.0) as i32) << 8
         | ((self.r * 255.0) as i32) << 16
         | ((self.a * 255.0) as i32) << 24;
      format!("{}", c)
   }
}

impl Colour {
   /// Create a colour from `u8`s
   pub const fn from_u8(r: u8, g: u8, b: u8, a: u8) -> Colour {
      Colour {
         r: r as f32 / 255.0,
         g: g as f32 / 255.0,
         b: b as f32 / 255.0,
         a: a as f32 / 255.0,
      }
   }
}
