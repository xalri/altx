
use esc::resources::*;
use esc::xml::*;
use altx::*;
use log::*;
use regex::*;
use simple_logger::*;
use structopt::*;

use std::path::*;
use std::fs::*;
use std::io::{Cursor, BufReader};
use altx::AnimatedPoly;

#[derive(Debug, StructOpt)]
#[structopt(about = "Converts altitude resource files")]
struct Opt {
   /// Input directory
   #[structopt(parse(from_os_str))]
   input: PathBuf,
   /// Output directory
   #[structopt(parse(from_os_str))]
   output: PathBuf,
}

fn main() {
   simple_logger::init_with_level(log::Level::Debug).unwrap();
   let opt = Opt::from_args();

   let input = Directory::open(opt.input);
   let output = Directory::open(opt.output);

   let pack_regex = regex::Regex::new("p[0-9]+").unwrap();

   for path in input.walk() {

      /*
      if path.starts_with(".image") {
         let _path = Path::new(&path);
         if let Some(name) = _path.file_name().map(|s| s.to_str().unwrap().to_string()) {
            let parent = _path.parent().unwrap();
            if pack_regex.is_match(&name) {
               let mut reader = Cursor::new(input.load(&path).unwrap());
               let atlas = TextureAtlas::from_reader(&mut reader).unwrap();
               // ...
            }
         }
      }
      */

      if path.starts_with(".poly") {
         if path.ends_with("0_128_5.animatedpoly") {
            println!("{}", path);
            let reader = Cursor::new(input.load(&path).unwrap());
            let poly = AnimatedPoly::from_reader(reader).unwrap();

            let mut iter = path.bytes().enumerate().rev().filter(|(_, c)| *c == b'/');
            let end = iter.next().unwrap().0;
            let start = iter.next().unwrap().0;
            let name = &path[start + 1 .. end];

            for f in &poly.frames {
               print!("\"{}_poly_{}\": [", name, f.delay_ms);
               for p in &f.poly.hull.points {
                  print!("(x:{},y:{}),", p.x, p.y);
               }
               println!("],");
            }

            println!("\"{}\": (", name);
            print!(   "frames: [ ");
            for f in &poly.frames {
               print!("({}, \"{}_poly_{}\"), ", (f.delay_ms as f32) / 10.0, name, f.delay_ms);
            }
            println!("],");
            println!("),");
         }
      }
   }
}
