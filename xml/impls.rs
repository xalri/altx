//! Implements Fragment and Attribute for various types

use std::collections::HashMap;
use std::hash::Hash;

use maplit::*;

use crate::error::*;
use crate::xml::element::*;
use crate::xml::fragment::*;

pub macro impl_attr($t:ty) {
   impl Attribute for $t {
      fn parse_attr(s: &str) -> Result<Self> {
         Ok(s.parse()?)
      }
      fn write_attr(&self) -> String {
         format!("{}", self)
      }
   }
}

pub macro impl_attrs($($t:ty),*) { $(impl_attr!($t);)* }

impl_attrs!(bool, i8, u8, i16, u16, i32, u32, i64, u64, f32, f64, String);

impl<T: Attribute> Attribute for Option<T> {
   /// Create a value of this type from an attribute string.
   fn parse_attr(s: &str) -> Result<Self> {
      Ok(Some(T::parse_attr(s)?))
   }

   /// Write this value to an attribute string.
   fn write_attr(&self) -> String {
      self.as_ref().unwrap().write_attr()
   }

   /// Add the field to an element builder.
   fn add_to_elem(&self, name: &str, e: &mut Element) {
      if self.is_some() {
         e.attributes.insert(name.to_string(), self.write_attr());
      }
   }
}

impl<T: Fragment> Fragment for Option<T> {
   fn from_element(e: &Element) -> Result<Self> {
      Ok(Some(T::from_element(e)?))
   }

   fn to_element(&self) -> Element {
      self.as_ref().unwrap().to_element()
   }

   fn add_to_elem(&self, name: &str, e: &mut Element) {
      if self.is_some() {
         let mut elem = self.to_element();
         elem.name = name.to_string();
         e.children.push(elem);
      }
   }
}

impl<T: Fragment> Fragment for Vec<T> {
   fn from_element(e: &Element) -> Result<Self> {
      e.children.iter().map(Fragment::from_element).collect()
   }

   fn to_element(&self) -> Element {
      Element {
         name: "list".to_string(),
         attributes: btreemap! {},
         children: self.iter().map(Fragment::to_element).collect(),
      }
   }
}

impl<K: Attribute + Eq + Hash, V: Attribute + Eq + Hash> Fragment for HashMap<K, V> {
   fn from_element(e: &Element) -> Result<Self> {
      e.children
         .iter()
         .map(|e| {
            Ok((
               K::find_in_parent("key", e)?.ok_or(err_msg("Map entry missing key"))?,
               V::find_in_parent("value", e)?.ok_or(err_msg("Map entry missing value"))?,
            ))
         })
         .collect()
   }

   fn to_element(&self) -> Element {
      let mut e = Element::new("Map");
      for (k, v) in self {
         let mut entry = Element::new("entry");
         k.add_to_elem("key", &mut entry);
         v.add_to_elem("value", &mut entry);
         e.children.push(entry);
      }
      e
   }
}

/// In a list of attributes the elements must be encoded as child nodes,
/// so primitive types must be 'boxed'.
/// This macro implements Fragment for Vec<T> where T is primitive by
/// encoding the elements in their boxed form.
/// Alternatively we could have wrapper structs `Integer(pub i32)`..
macro_rules! prim_list {
   ($t:ty, $name:expr) => {
      impl Fragment for Vec<$t> {
         fn from_element(e: &Element) -> Result<Self> {
            e.children
               .iter()
               .map(|e| {
                  e.attributes
                     .get("value")
                     .ok_or(err_msg("Missing value attr"))
                     .and_then(|value| value.parse::<$t>().map_err(|e| e.into(): Error))
               })
               .collect::<Result<_>>()
         }

         fn to_element(&self) -> Element {
            Element {
               name: "list".to_string(),
               attributes: btreemap! {},
               children: self
                  .iter()
                  .map(|e| Element {
                     name: $name.to_string(),
                     attributes: btreemap! {
                        "value".to_string() => e.to_string(),
                     },
                     children: vec![],
                  })
                  .collect(),
            }
         }
      }
   };
}

prim_list!(String, "String");
// prim_list!(i32, "Integer");
// ...
