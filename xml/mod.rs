//! A small XML library with `#[derive]`.

mod element;
mod fragment;
mod impls;
#[cfg(test)]
mod tests;

// re-export the derive macros.
pub use codegen::*;

pub use self::element::*;
pub use self::fragment::*;
pub use self::impls::*;
