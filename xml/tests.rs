#![allow(non_snake_case)]

use crate::error::*;
use crate::xml::*;

#[derive(Fragment, Debug, PartialEq)]
pub struct PowerupSpawner {
   // Each field must implement one of Attribute or Fragment.
   // A default impl for Attribute exists for types implementing
   // `FromStr` and `Display`. Names are converted to mixedCase
   #[xml(default = "true")]
   never_fade: bool,

   // Attributes can have default values which are used if the
   // field isn't found. If no default is specified then the
   // field being missing is an error.
   #[xml(default = "10.0")]
   spawn_time: f32,

   // A child Fragment, the tag name is the name of the field.
   #[xml(required)]
   team: Team,

   // A collection of Fragments, encoded as an element with the field
   // name containing children of all the elements, where the inner
   // elements have the name of the type.
   selected_powerups: Vec<String>,
}

// Enums are encoded as an element with an attribute giving their integer
// discriminant (Team::Spectator <-> <Team index="2" \>)
#[derive(Fragment, Debug, PartialEq)]
pub enum Team {
   A,
   B,
   Spectator, // etc...
}

// If the xml_default attribute is set then Default will be derived using
// the xml defaults.
#[derive(Fragment)]
#[xml(impl_default)]
pub struct DefaultTest {
   #[xml(default = "1.0")]
   x: f32,
}

#[test]
fn run() {
   assert_eq!(DefaultTest::default().x, 1.0);

   let team_str = r#"<Team index="2" />"#;
   assert_eq!(Team::from_xml(team_str).unwrap(), Team::Spectator);
   assert_eq!(Team::Spectator.to_xml().as_str(), team_str);

   let xml_str = r#"
     <powerupSpawners spawnTime="20.0">
       <team index="2" />
       <selectedPowerups>
         <String value="Homing Missile" />
         <String value="Wall" />
       </selectedPowerups>
     </powerupSpawners>
   "#;

   let value = PowerupSpawner::from_xml(xml_str).unwrap();
   let cycled = value.to_xml();
   assert_eq!(value, PowerupSpawner::from_xml(cycled.as_str()).unwrap());
}
