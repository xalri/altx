//! Provides traits for converting between data types and XML.

use std::io::{BufRead, Write};

use crate::error::*;
use crate::xml::element::*;

/// An item which can be represented as an XML element.
pub trait Fragment: Sized {
   /// Read a value from an XML element.
   fn from_element(e: &Element) -> Result<Self>;

   /// Create an XML element representing the value.
   fn to_element(&self) -> Element;

   /// Parse an xml string into this type.
   fn from_xml(xml: &str) -> Result<Self> {
      Self::from_element(&xml.parse()?)
   }

   /// Encode the value as an XML string
   fn to_xml(&self) -> String {
      (&self.to_element()).into()
   }

   /// Add the field as a child to an element
   fn add_to_elem(&self, name: &str, e: &mut Element) {
      let mut elem = self.to_element();
      elem.name = name.to_string();
      e.children.push(elem);
   }

   /// Find this field from it's parent element
   fn find_in_parent(name: &str, e: &Element) -> Result<Option<Self>> {
      match e.children.iter().find(|e| e.name.to_lowercase() == name.to_lowercase()) {
         Some(e) => Ok(Some(Self::from_element(e)?)),
         None => Ok(None),
      }
   }

   /// Find the field in it's parent element, using a default value if it's not found
   fn find_or(name: &str, e: &Element, default: Self) -> Result<Self> {
      Ok(Self::find_in_parent(name, e)?.unwrap_or(default))
   }

   /// Parse XML from a reader
   fn from_reader<R: BufRead>(reader: R) -> Result<Self> {
      use quick_xml::Reader as EventReader;
      let mut ev_reader = EventReader::from_reader(reader);
      let elem = Element::from_reader(&mut ev_reader)?;
      Self::from_element(&elem)
   }

   /// Parse XML from a byte slice
   fn from_slice(data: &[u8]) -> Result<Self> {
      Self::from_reader(std::io::Cursor::new(data))
   }

   /// Write the XML
   fn write_to<W: Write>(&self, writer: &mut W) -> Result<()> {
      let elem = self.to_element();
      elem.write_to(writer)?;
      Ok(())
   }
}

/// An item which can be represented as an XML attribute, automatically
/// implemented for types which implement `FromStr` and `Display`.
pub trait Attribute: Sized {
   /// Create a value of this type from an attribute string.
   fn parse_attr(s: &str) -> Result<Self>;

   /// Write this value to an attribute string.
   fn write_attr(&self) -> String;

   /// Update this value from an attribute string.
   fn read_attr(&mut self, s: &str) -> Result<()> {
      *self = Self::parse_attr(s)?;
      Ok(())
   }

   /// Add the field to an element builder.
   fn add_to_elem(&self, name: &str, e: &mut Element) {
      e.attributes.insert(name.to_string(), self.write_attr());
   }

   /// Read the field from it's parent element
   fn find_in_parent(name: &str, e: &Element) -> Result<Option<Self>> {
      if let Some(value) = e.attributes.get(name) {
         Ok(Some(Self::parse_attr(value)?))
      } else {
         Ok(None)
      }
   }

   /// Find the field in it's parent element, using a default value if it's not found
   fn find_or(name: &str, e: &Element, default: Self) -> Result<Self> {
      Ok(Self::find_in_parent(name, e)?.unwrap_or(default))
   }
}
