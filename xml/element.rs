/*
 * Based on [minidom-rs](https://gitlab.com/lumi/minidom-rs).
 * Copyright 2017 minidom-rs contributors
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

//! Provides an `Element` type, which represents DOM nodes.

use std::collections::BTreeMap;
use std::io::BufRead;
use std::io::Write;
use std::str;
use std::str::FromStr;

use maplit::*;
use quick_xml::events::{BytesStart, Event};
use quick_xml::Reader as EventReader;

use crate::error::*;

/// Escape XML text
pub fn write_escaped<W: Write>(writer: &mut W, input: &str) -> Result<()> {
   for c in input.chars() {
      match c {
         '&' => write!(writer, "&amp;")?,
         '<' => write!(writer, "&lt;")?,
         '>' => write!(writer, "&gt;")?,
         '\'' => write!(writer, "&apos;")?,
         '"' => write!(writer, "&quot;")?,
         _ => write!(writer, "{}", c)?,
      }
   }

   Ok(())
}

/// A struct representing a DOM Element.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Element {
   /// The element's name
   pub name: String,
   /// The attributes
   pub attributes: BTreeMap<String, String>,
   /// Child elements
   pub children: Vec<Element>,
}

impl<'a> From<&'a Element> for String {
   fn from(elem: &'a Element) -> String {
      let mut writer = Vec::new();
      elem.write_to(&mut writer).unwrap();
      String::from_utf8(writer).unwrap()
   }
}

impl FromStr for Element {
   type Err = Error;

   fn from_str(s: &str) -> Result<Element> {
      let mut reader = EventReader::from_str(s);
      Element::from_reader(&mut reader)
   }
}

impl Element {
   /// Create an element with the given name
   pub fn new(name: &str) -> Element {
      Element {
         name: name.to_string(),
         attributes: btreemap![],
         children: vec![],
      }
   }

   pub fn attr(&self, name: &str) -> &str {
      self.attributes.get(name).map(String::as_str).unwrap_or("")
   }

   /// Parse a document from an `EventReader`.
   pub fn from_reader<R: BufRead>(reader: &mut EventReader<R>) -> Result<Element> {
      let mut buf = Vec::new();

      let root: Element = loop {
         let e = reader.read_event(&mut buf)?;
         match e {
            Event::Empty(ref e) | Event::Start(ref e) => {
               break build_element(reader, e)?;
            }
            Event::Eof => bail!("the end of the document has been reached prematurely"),
            _ => (), // TODO: may need more errors
         }
      };

      let mut stack = vec![root];

      loop {
         match reader.read_event(&mut buf)? {
            Event::Empty(ref e) => {
               let elem = build_element(reader, e)?;
               // Since there is no Event::End after, directly append it to the current node
               stack.last_mut().unwrap().children.push(elem);
            }
            Event::Start(ref e) => {
               let elem = build_element(reader, e)?;
               stack.push(elem);
            }
            Event::End(ref e) => {
               if stack.len() <= 1 {
                  break;
               }
               let elem = stack.pop().unwrap();
               if let Some(to) = stack.last_mut() {
                  if elem.name.as_bytes() != e.name() {
                     bail!("The XML is invalid, an element was wrongly closed")
                  }
                  to.children.push(elem);
               }
            }
            Event::Text(_) | Event::CData(_) => (),
            Event::Eof => {
               break;
            }
            _ => (), // TODO: may need to implement more
         }
      }
      Ok(stack.pop().unwrap())
   }

   /// Output a document to a `Writer` (without an `<?xml?>` prelude)
   pub fn write_to<W: Write>(&self, writer: &mut W) -> Result<()> {
      write!(writer, "<{}", self.name)?;

      for (key, value) in &self.attributes {
         write!(writer, " {}=\"", key)?;
         write_escaped(writer, value)?;
         write!(writer, "\"")?;
      }

      if self.children.is_empty() {
         write!(writer, " />")?;
      } else {
         write!(writer, ">")?;

         for child in &self.children {
            child.write_to(writer)?;
         }

         write!(writer, "</{}>", self.name)?;
      }
      Ok(())
   }
}

fn build_element<R: BufRead>(reader: &EventReader<R>, event: &BytesStart) -> Result<Element> {
   let attributes = event
      .attributes()
      .map(|o| {
         let o = o?;
         let key = str::from_utf8(o.key)?.to_owned();
         let value = o.unescape_and_decode_value(reader)?;
         Ok((key, value))
      })
      .collect::<Result<BTreeMap<String, String>>>()?;

   let name = str::from_utf8(event.name())?.to_owned();
   let element = Element {
      name,
      attributes,
      children: vec![],
   };
   Ok(element)
}
