//! Parsing, modification, and querying of altitude map and resources files.

#![feature(type_ascription)]
#![feature(decl_macro)]
#![feature(const_fn)]

mod altx;
mod atlas;
mod common;
mod map;
mod sprites;
mod xml;

pub mod error {
   pub use failure::*;
   pub type Result<T> = std::result::Result<T, failure::Error>;
}

pub use self::altx::*;
pub use self::atlas::*;
pub use self::common::*;
pub use self::map::*;
pub use self::sprites::*;
pub use self::xml::*;

use self::error::*;
use byteorder::LittleEndian as LE;
use byteorder::{ReadBytesExt, WriteBytesExt};
use std::io;

/// Read a utf8 `String` from a stream.
pub fn read_string(from: &mut dyn io::Read) -> Result<String> {
   let len = from.read_u16::<LE>()? as usize;
   let mut buf = vec![0u8; len];
   from.read_exact(&mut buf)?;
   Ok(String::from_utf8(buf)?)
}

/// Write a utf8 `String` to a stream.
pub fn write_string(to: &mut dyn io::Write, value: &str) -> Result<()> {
   let bytes = value.as_bytes();
   to.write_u16::<LE>(bytes.len() as u16)?;
   to.write_all(bytes)?;
   Ok(())
}
