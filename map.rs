use crate::altx::*;
use crate::common::*;
use crate::error::*;
use crate::sprites::*;
use crate::xml::*;

/// A map loaded from an altx file
pub struct Map {
   pub level: Level,
   pub altx: Altx,
}

impl Map {
   /// Load a map from a reader
   pub fn from_reader(r: impl std::io::Read) -> Result<Map> {
      let altx = Altx::from_reader(r)?;
      for file in altx.files() {
         if file.path.ends_with("alte") {
            return Ok(Map {
               level: Level::from_slice(file.data)?,
               altx,
            });
         }
      }
      bail!("Couldn't find an alte file in the archive")
   }
}

/// Low-level representation of altitude's XML map format.
#[derive(Debug, PartialEq, Fragment)]
#[xml(impl_default)]
pub struct Level {
   /// The colour of the background
   #[xml(default = "DEFAULT_BG_COLOR")]
   pub background_color: Colour,

   /// The colour used for player names.
   #[xml(default = "DEFAULT_TEXT_COLOR")]
   pub name_text_color: Colour,

   /// Use black smoke rather than grey, for maps with a light background.
   #[xml(default = "true")]
   pub use_black_smoke_for_plane_damage: bool,

   /// True if the screen should be cleared with the background colour prior to
   /// drawing a new frame.
   #[xml(default = "true")]
   pub background_clear_required: bool,

   /// A collection of views, each view can contain geometry, spawn points,
   /// powerups, and so on. Each map must have a view named "Game" which defines
   /// the size of the map.
   pub views: Vec<View>,

   /// Per-level configuration for objective game modes.
   pub level_mode_config: LevelModeConfig,
}

#[derive(Debug, PartialEq, Fragment)]
#[xml(impl_default)]
pub struct LevelModeConfig {
   #[xml(default = "120")]
   pub objective_game_time_seconds: i32,
}

#[derive(Debug, PartialEq, Fragment, Default)]
pub struct View {
   pub name: String,
   pub size: Dimensions,
   pub bounds: Bounds,

   #[xml(name = "Base")]
   pub bases: Vec<Base>,
   #[xml(name = "Goal")]
   pub goals: Vec<Goal>,
   #[xml(name = "Hoop")]
   pub hoops: Vec<Hoop>,
   #[xml(name = "objectiveDescriptor")]
   pub objectives: Vec<Objective>,
   #[xml(name = "powerupSpawner")]
   pub spawners: Vec<PowerupSpawner>,
   #[xml(name = "spawnPoints")]
   pub spawn_points: Vec<SpawnPoint>,
   #[xml(name = "geometry")]
   pub geometry: Vec<Geometry>,
   #[xml(name = "Turret")]
   pub turrets: Vec<Turret>,
}

#[derive(Debug, PartialEq, Fragment)]
pub struct Base {
   pub team: Team,
   pub layer: i32,
   pub x: i32,
   pub y: i32,
   pub transformation: TransformComponents,
}

#[derive(Debug, PartialEq, Fragment)]
pub struct Goal {
   pub team: Team,
   pub layer: i32,
   pub x: i32,
   pub y: i32,
   pub transformation: TransformComponents,
}

#[derive(Debug, PartialEq, Fragment)]
pub struct Hoop {
   pub layer: i32,
   pub x: i32,
   pub y: i32,
   pub transformation: TransformComponents,
   pub objective_id: String,
}

#[derive(Debug, PartialEq, Fragment)]
pub struct Objective {
   pub x: i32,
   pub y: i32,
   pub orientation: f32,
   pub name: String,
   pub description: String,
}

#[derive(Debug, PartialEq, Fragment)]
pub struct PowerupSpawner {
   pub team: Team,
   pub layer: i32,
   pub x: i32,
   pub y: i32,
   pub orientation: f32,
   pub spawn_time: f32,
   pub selected_powerups: Vec<String>,
   pub never_fade: bool,
   pub spawn_at_round_start: bool,
}

#[derive(Debug, PartialEq, Fragment)]
pub struct SpawnPoint {
   pub team: Team,
   pub layer: i32,
   pub x: i32,
   pub y: i32,
   pub orientation: f32,
}

#[derive(Debug, PartialEq, Fragment)]
#[xml(name = "geometry")]
pub struct Geometry {
   pub layer: i32,
   pub x: i32,
   pub y: i32,
   pub orientation: f32,
   pub visible: bool,
   pub collidable: bool,
   pub elasticity: f32,
   pub damage_modifier: f32,
   #[xml(required)]
   pub textured_poly: TexturedPoly,
}

#[derive(Debug, PartialEq, Fragment)]
pub struct Turret {
   pub team: Team,
   pub layer: i32,
   pub x: i32,
   pub y: i32,
   pub transformation: TransformComponents,
   #[xml(default = "45.0")]
   pub turn_left: f32,
   #[xml(default = "45.0")]
   pub turn_right: f32,
   #[xml(default = "600.0")]
   pub max_distance: f32,
   #[xml(default = "600")]
   pub health: i32,
   #[xml(default = "0.6")]
   pub gun_cooldown_seconds: f32,
   #[xml(default = "17.0")]
   pub bullet_speed: f32,
   #[xml(default = "10")]
   pub bullet_direct_damage: i32,
   #[xml(default = "30")]
   pub bullet_blast_damage: i32,
   #[xml(default = "80")]
   pub bullet_blast_radius: i32,
}

/// The game mode of a map.
#[derive(PartialEq, Debug, Clone, Copy)]
pub enum MapMode {
   TeamBaseDestruction,
   OneLifeBaseDestruction,
   OneLifeDemolition,
   OneLifeDeathmatch,
   TeamDeathmatch,
   FreeForAll,
   Ball,
}
