use darling::*;
use heck::*;
use proc_macro::TokenStream;
use syn::*;

#[derive(FromDeriveInput)]
#[darling(attributes(xml))]
pub struct FragmentOpts {
   #[darling(default)]
   impl_default: bool,
   #[darling(default)]
   name: Option<String>,
}

#[derive(FromField)]
#[darling(attributes(xml))]
pub struct FieldOpts {
   #[darling(default)]
   flat_vec: bool,
   #[darling(default)]
   required: bool,
   #[darling(default)]
   name: Option<String>,
   #[darling(default)]
   default: Option<String>,
}

pub fn derive_fragment_impl(input: TokenStream) -> TokenStream {
   let ast: DeriveInput = parse(input).unwrap();
   let opts = FragmentOpts::from_derive_input(&ast).unwrap();
   let name = ast.ident;
   let xml_name = opts.name.clone().unwrap_or(name.to_string());

   match ast.data {
      Data::Struct(s) => fragment_struct(s, name, xml_name, opts.impl_default),
      Data::Enum(e) => {
         if opts.impl_default {
            panic!("#[derive(Fragment)]: cannot impl_default for enum")
         }
         fragment_enum(e, name, xml_name)
      }
      _ => unimplemented!(),
   }
}

pub fn fragment_enum(e: DataEnum, name: Ident, xml_name: String) -> ::proc_macro::TokenStream {
   for v in &e.variants {
      match v.fields {
         Fields::Unit => (),
         _ => unimplemented!(),
      }
   }

   let variants = e
      .variants
      .iter()
      .map(|var| &var.ident)
      .map(|ident| quote!(#name::#ident))
      .collect::<Vec<_>>();
   let _variants = variants.clone();

   let discriminants = (0..e.variants.len())
      .map(|i| i.to_string())
      .map(|i| quote!(#i))
      .collect::<Vec<_>>();
   let _discriminants = discriminants.clone();

   quote!(
        impl Fragment for #name {
           fn from_element(e: &Element) -> Result<Self> {
              let value = e.attributes.get("index")
                 .ok_or(err_msg("Missing value attr"))?;
              Ok(match value.as_str() {
                 #(#discriminants => #variants),*,
                 x => {
                    let err = format_err!("Unknown {} variant {}", stringify!(#name), x);
                    return Err(err);
                 }
              })
           }

           fn to_element(&self) -> Element {
              let value = match *self {
                 #(#_variants => #_discriminants),*
              }.to_string();
              let mut attrs = ::std::collections::BTreeMap::new();
              attrs.insert("index".to_string(), value);

              Element {
                 name: #xml_name.to_string(),
                 attributes: attrs,
                 children: vec![],
              }
           }
        }
    )
   .into()
}

pub fn fragment_struct(e: DataStruct, name: Ident, xml_name: String, impl_default: bool) -> ::proc_macro::TokenStream {
   #[derive(Debug)]
   struct Field {
      ty: Type,
      name: Ident,
      // The field name in the XML representation.
      xml_name: String,
      // the field's default value, either set by #[xml(default: ..)]; set by
      // the Default trait; or `None` if the field is marked #[xml(required: true)]
      default: Option<Expr>,
      flat_vec: bool,
   }

   let fields = match e.fields {
      Fields::Named(fields) => fields,
      _ => unimplemented!(),
   };

   let fields = fields
      .named
      .into_iter()
      .map(|field| {
         let opts = FieldOpts::from_field(&field).unwrap();
         let name = field.ident.unwrap();
         let ty = field.ty;

         Field {
            xml_name: opts.name.unwrap_or(name.to_string().to_mixed_case()),
            flat_vec: opts.flat_vec,
            default: opts
               .default
               .or(if opts.required {
                  None
               } else {
                  Some("::std::default::Default::default()".to_string())
               })
               .map(|s| parse_str(&s).unwrap()),
            name,
            ty,
         }
      })
      .collect::<Vec<_>>();

   // An iterator over fields which outputs the tokens to read the field from an element `e`.
   let read_fields = fields.iter().map(|field| {
      let field_name = &field.name;
      let xml_name = &field.xml_name;
      let ty = &field.ty;

      // The value to return if the field isn't found in the element
      let fallback = field.default.clone().map(|value| quote!{ #value }).unwrap_or_else(|| {
         quote!{
            let err = format_err!("Missing required field '{}' in '{}'.", #xml_name, stringify!(#name));
            return Err(err);
         }
      });

      if field.flat_vec {
         return quote!{
            #field_name: e.children.iter()
               .filter(|e| &e.name == #xml_name)
               .map(Fragment::from_element).collect::<Result<Vec<_>>>()?
         };
      }

      quote!{
         #field_name: match <#ty>::find_in_parent(#xml_name, e)? {
            Some(value) => { value }
            None => { #fallback }
         }
      }
   });

   // An iterator over fields which outputs the tokens to write the field to an element `e`.
   let write_fields = fields
      .iter()
      .map(|field| {
         let field_name = &field.name;
         let xml_name = &field.xml_name;

         if field.flat_vec {
            return quote!{
               for v in &self.#field_name { v.add_to_elem(#xml_name, &mut e) }
            };
         }

         quote! { self.#field_name.add_to_elem(#xml_name, &mut e); }
      })
      .collect::<Vec<_>>();

   // Implements fragment using read_fields & write_fields
   let impl_fragment = quote! {
      impl Fragment for #name {
         fn from_element(e: &Element) -> Result<Self> {
            Ok(#name { #(#read_fields),* })
         }

         fn to_element(&self) -> Element {
            let mut e = Element::new(#xml_name);
            #({#write_fields})*
            e
         }
      }
   };

   // Implements default using #[default="..."] values. Enabled using the
   // #[xml_default] attribute on the struct.
   let impl_default = if impl_default {
      let fields = fields.iter().map(|field| {
         let field_name = &field.name;
         if let Some(ref value) = field.default {
            quote!{ #field_name: #value }
         } else {
            quote!{ #field_name: Default::default() }
         }
      });

      Some(quote!{
         impl ::std::default::Default for #name {
            fn default() -> #name { #name { #(#fields),* } }
         }
      })
   } else {
      None
   };

   // Combine everything
   quote!(
       #impl_default
       #impl_fragment
    )
   .into()
}
