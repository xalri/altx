#![recursion_limit = "128"]

extern crate darling;
extern crate proc_macro;
extern crate proc_macro2;
extern crate syn;
#[macro_use]
extern crate quote;
extern crate heck;

mod xml;
use xml::*;

#[proc_macro_derive(Fragment, attributes(xml))]
pub fn derive_fragment(input: ::proc_macro::TokenStream) -> ::proc_macro::TokenStream {
   derive_fragment_impl(input)
}
