use altx::error::*;
use altx::*;
use std::ffi::*;
use std::fs::*;
use std::io::BufReader;
use std::path::*;

#[test]
fn verify_maps() {
   let dir = read_dir("./tests/maps/").unwrap();
   let mut failed = 0;

   for dir_entry in dir {
      let file = dir_entry.unwrap();
      if file.path().extension() != Some(OsStr::new("altx")) {
         continue;
      }

      fn load_map<P: AsRef<Path>>(path: P) -> Result<()> {
         let path = path.as_ref();
         Map::from_reader(File::open(path)?)?;
         Ok(())
      }

      if let Err(e) = load_map(file.path()) {
         println!("Error reading map at {:?}: {}", file.path(), e);
         failed += 1;
      }
   }

   if failed > 0 {
      panic!("{} maps failed", failed)
   }
}

#[test]
fn alte_parse_correctness() {
   let file = BufReader::new(File::open("tests/map.alte").unwrap());
   let actual = Level::from_reader(file).unwrap();

   let expected = Level {
      //name: "ffa_map.alte".to_string(),
      background_color: Colour::from_u8(0x30, 0x30, 0x30, 0xFF),
      name_text_color: Colour::from_u8(0xE0, 0xE0, 0xE0, 0xFF),
      background_clear_required: false,
      use_black_smoke_for_plane_damage: false,
      level_mode_config: LevelModeConfig {
         objective_game_time_seconds: 500,
      },
      views: vec![View {
         name: "Game".to_string(),
         size: Dimensions {
            width: 2000.0,
            height: 1000.0,
         },
         bounds: Bounds {
            minX: 0.0,
            maxX: 2000.0,
            minY: 0.0,
            maxY: 1000.0,
         },
         geometry: vec![Geometry {
            x: 1000,
            y: 100,
            layer: 1,
            orientation: 0.0,
            visible: true,
            collidable: true,
            elasticity: 0.125,
            damage_modifier: 1.0,
            textured_poly: TexturedPoly {
               hull: Hull {
                  points: vec![Vec2::new(0.0, 0.0), Vec2::new(-400.0, 800.0), Vec2::new(400.0, 800.0)],
                  colors: vec![
                     Colour::from_u8(0xFF, 0x00, 0x00, 0xFF),
                     Colour::from_u8(0x00, 0xFF, 0x00, 0xFF),
                     Colour::from_u8(0x00, 0x00, 0xFF, 0xFF),
                  ],
               },
               texture: Some(TransformedImage {
                  image: Image {
                     path: "test".to_string(),
                  },
                  xform: TransformComponents {
                     scale: 2.0,
                     orientation: 2.0,
                     flip_x: true,
                     flip_y: true,
                     position: Vec2 { x: -10.0, y: 10.0 },
                  },
               }),
            },
         }],
         spawn_points: vec![
            SpawnPoint {
               x: 100,
               y: 100,
               layer: 1,
               orientation: 20.0,
               team: Team::Red,
            },
            SpawnPoint {
               x: 1900,
               y: 100,
               layer: 1,
               orientation: -20.0,
               team: Team::Blue,
            },
         ],
         bases: vec![
            Base {
               x: 500,
               y: 600,
               layer: 1,
               team: Team::Red,
               transformation: TransformComponents {
                  scale: 0.8,
                  orientation: -45.0,
                  flip_x: false,
                  flip_y: false,
                  position: Vec2 { x: 0.0, y: 0.0 },
               },
            },
            Base {
               x: 1500,
               y: 600,
               layer: 1,
               team: Team::Blue,
               transformation: TransformComponents {
                  scale: 0.8,
                  orientation: 45.0,
                  flip_x: false,
                  flip_y: false,
                  position: Vec2 { x: 0.0, y: 0.0 },
               },
            },
         ],
         objectives: Vec::new(),
         turrets: vec![
            Turret {
               x: 500,
               y: 550,
               layer: 1,
               team: Team::Red,
               turn_left: 50.0,
               turn_right: 50.0,
               max_distance: 500.0,
               transformation: TransformComponents {
                  scale: 2.0,
                  orientation: 0.0,
                  flip_x: true,
                  flip_y: false,
                  position: Vec2 { x: 0.0, y: 0.0 },
               },
               health: 100,
               gun_cooldown_seconds: 0.1,
               bullet_speed: 20.0,
               bullet_direct_damage: 50,
               bullet_blast_damage: 0,
               bullet_blast_radius: 0,
            },
            Turret {
               x: 1500,
               y: 550,
               layer: 1,
               team: Team::Blue,
               turn_left: 50.0,
               turn_right: 50.0,
               max_distance: 500.0,
               transformation: TransformComponents {
                  scale: 2.0,
                  orientation: 0.0,
                  flip_x: true,
                  flip_y: false,
                  position: Vec2 { x: 0.0, y: 0.0 },
               },
               health: 100,
               gun_cooldown_seconds: 0.1,
               bullet_speed: 20.0,
               bullet_direct_damage: 50,
               bullet_blast_damage: 0,
               bullet_blast_radius: 0,
            },
         ],
         hoops: Vec::new(),
         spawners: Vec::new(),
         goals: Vec::new(),
      }],
   };

   assert_eq!(expected, actual);
}
