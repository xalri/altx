//! The graphics resource pack.

use std::default::Default;
use std::fs;
use std::io::Cursor;
use std::io::{Read, Write};
use std::path::Path;

use byteorder::BigEndian as BE;
use byteorder::LittleEndian as LE;
use byteorder::{ReadBytesExt, WriteBytesExt};
use image::imageops;
use image::load;
use image::{DynamicImage, GenericImage, ImageFormat, RgbaImage};
use inflate::*;

use crate::error::*;

/// A texture atlas; A group of sprites stored in a single image.
#[derive(Clone)]
pub struct TextureAtlas {
   /// The file names and positions of sprites in the image.
   pub regions: Vec<Region>,
   /// Image options.
   pub settings: AtlasSettings,
   /// The actual image data.
   pub textures: Vec<DynamicImage>,
}

/// A single region of a texture atlas.
#[derive(Debug, Clone)]
pub struct Region {
   pub name: String,
   pub off_x: u32,
   pub off_y: u32,
   pub width: u32,
   pub height: u32,
}

/// Settings for a texture atlas
#[derive(Debug, Clone)]
pub struct AtlasSettings {
   pub compression: Compression,
   pub format: PixelFormat,
   pub repeat: bool,
   pub filter: bool,
   pub max_width: u32,
   pub max_height: u32,
}

/// Image compression format.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Compression {
   Uncompressed = 10000,
   DXT = 10001,
   JPEG = 10002,
   LZMA = 10003,
}

/// Image pixel format.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PixelFormat {
   ALPHA8 = 0,
   RGB5 = 1,
   RGBA4 = 2,
   RGBA8 = 3,
   DXT1 = 4,
   DXT1A = 5,
   DXT3 = 6,
   DXT5 = 7,
}

impl TextureAtlas {
   /// Load a `TextureAtlas` from a reader.
   pub fn from_reader<R: Read>(mut r: R) -> Result<Self> {
      let regions: Result<Vec<_>> = (0..r.read_u32::<LE>()?).map(|_| Region::read(&mut r)).collect();

      let regions = regions?;
      let settings = AtlasSettings::read(&mut r)?;
      let mut textures = Vec::new();

      let len = r.read_u32::<BE>()?;

      for _ in 0..len {
         let w = r.read_u32::<BE>()?;
         let h = r.read_u32::<BE>()?;

         match settings.compression {
            Compression::Uncompressed => {
               textures.push(read_raw(w, h, settings.format, &mut r)?);
            }
            Compression::JPEG => {
               textures.push(read_jpeg(&mut r, w, h)?);
            }
            x => bail!("Unsupported image compression format: {:?}", x),
         }
      }
      Ok(TextureAtlas {
         regions,
         settings,
         textures,
      })
   }

   /// Export all the images in the pack to the given directory
   pub fn save_images(&self, path: &Path) -> Result<()> {
      for s in &self.regions {
         let path = path.join(&s.name);
         if let Some(p) = path.parent() {
            fs::create_dir_all(p)?;
         }
         s.make_image(&self.textures).save(&path)?;
      }
      Ok(())
   }
}

impl Region {
   /// Read a region's metadata from a reader.
   pub fn read(r: &mut dyn Read) -> Result<Region> {
      Ok(Region {
         name: crate::read_string(r)?,
         off_x: r.read_u32::<LE>()?,
         off_y: r.read_u32::<LE>()?,
         width: r.read_u32::<LE>()?,
         height: r.read_u32::<LE>()?,
      })
   }

   /// Write the region metadata.
   pub fn write<W: Write>(&self, w: &mut W) -> Result<()> {
      crate::write_string(w, &self.name)?;
      w.write_u32::<LE>(self.off_x)?;
      w.write_u32::<LE>(self.off_y)?;
      w.write_u32::<LE>(self.width)?;
      w.write_u32::<LE>(self.height)?;
      Ok(())
   }

   /// Get the region from the given textures as a new image.
   pub fn make_image(&self, textures: &Vec<DynamicImage>) -> RgbaImage {
      if textures.len() == 1 {
         let mut tx = textures[0].to_rgba();
         let y = tx.dimensions().1 - self.off_y - self.height;
         tx.sub_image(self.off_x, y, self.width, self.height).to_image()
      } else if textures.len() > 1 {
         let mut image = RgbaImage::new(self.width, self.height);
         let mut x = 0;
         let mut y = 0;

         for tx in textures {
            let tx_w = tx.dimensions().0;
            let tx_h = tx.dimensions().1;
            if self.height - y >= tx_h {
               let target_y = self.height - y - tx_h;
               imageops::overlay(&mut image, &tx.to_rgba(), x, target_y);
            } else {
               let start_y = tx_h - (self.height - y);
               let sub_img = tx.to_rgba().sub_image(0, start_y, tx_w, self.height - y).to_image();
               imageops::overlay(&mut image, &sub_img, x, 0);
            }

            x += tx.dimensions().0;
            if x >= self.width {
               x = 0;
               y += tx_h;
            }
         }

         image
      } else {
         panic!("make_image called with no textures");
      }
   }
}

impl AtlasSettings {
   /// Read the settings from a reader, using default for any non-encoded values.
   pub fn read(r: &mut dyn Read) -> Result<AtlasSettings> {
      let compression = r.read_u32::<BE>()?;
      let format = if compression < 10000 {
         compression
      } else {
         r.read_u32::<BE>()?
      };

      Ok(AtlasSettings {
         compression: compression.into(),
         format: format.into(),
         repeat: r.read_u32::<BE>()? == 1,
         filter: r.read_u32::<BE>()? == 1,
         ..Default::default()
      })
   }

   /// Write some of the settings to a writer.
   pub fn write<W: Write>(&self, w: &mut W) -> Result<()> {
      w.write_u32::<BE>(self.compression as u32)?;
      w.write_u32::<BE>(self.format as u32)?;
      w.write_u32::<BE>(if self.repeat { 1 } else { 0 })?;
      w.write_u32::<BE>(if self.filter { 1 } else { 0 })?;
      Ok(())
   }
}

/// Read uncompressed image data
fn read_raw(w: u32, h: u32, format: PixelFormat, r: &mut dyn Read) -> Result<DynamicImage> {
   let mut data = vec![0u8; (w * h * 4) as usize];
   r.read_exact(&mut data)?;
   match format {
      PixelFormat::RGBA8 => {
         // Unwrap is safe as data is guaranteed to be large enough.
         let rgba = RgbaImage::from_raw(w, h, data).unwrap();
         Ok(DynamicImage::ImageRgba8(rgba))
      }
      x => bail!("Unsupported pixel format: {:?}", x),
   }
}

/// Read a JPEG image with an optional alpha mask
fn read_jpeg(r: &mut dyn Read, _w: u32, _h: u32) -> Result<DynamicImage> {
   let len = r.read_u32::<BE>()?;

   let mut data = vec![0u8; len as usize];
   r.read_exact(&mut data)?;

   let mut stream = Cursor::new(data);
   let a = stream.read_u32::<BE>()?;
   let alpha_bits = a & 0xF;
   let lzma = (a & 0x20) != 0;
   let mut mask = Vec::new();

   if alpha_bits != 0 {
      if lzma {
         let mut decoder = lzma::read(stream).unwrap();
         decoder.read_to_end(&mut mask)?;
         stream = decoder.into_inner();

      /*
      use xz2
      let s = xz2::stream::Stream::new_lzma_decoder(::std::u64::MAX)?;
      let mut decoder = xz2::read::XzDecoder::new_stream(stream, s);
      let mut raw = Vec::new();
      decoder.read_to_end(&mut raw)?;
      // TODO: what's going on here?..
      stream = Cursor::new(raw);
      */
      } else {
         let len = stream.read_u32::<BE>()?;
         let mut data = vec![0u8; len as usize];
         stream.read_exact(&mut data)?;
         mask = inflate_bytes_zlib(&data).map_err(|e| err_msg(e))?;
      }
   }

   let image = load(stream, ImageFormat::JPEG)?;

   if mask.is_empty() {
      Ok(image)
   } else {
      let rgba = image.to_rgba();
      let (w, h) = rgba.dimensions();
      let mut pixels = rgba.into_raw();

      for i in 0..mask.len() {
         if (i << 2) + 3 >= pixels.len() {
            break;
         }
         pixels[(i << 2) + 3] = mask[i];
      }

      Ok(DynamicImage::ImageRgba8(RgbaImage::from_raw(w, h, pixels).unwrap()))
   }
}

impl Default for AtlasSettings {
   fn default() -> Self {
      AtlasSettings {
         compression: Compression::LZMA,
         format: PixelFormat::RGBA8,
         repeat: false,
         filter: false,
         max_width: 512,
         max_height: 512,
      }
   }
}

impl From<u32> for Compression {
   fn from(x: u32) -> Self {
      match x {
         10001 => Compression::DXT,
         10002 => Compression::JPEG,
         10003 => Compression::LZMA,
         _ => Compression::Uncompressed,
      }
   }
}

impl From<u32> for PixelFormat {
   fn from(x: u32) -> Self {
      match x {
         1 => PixelFormat::RGB5,
         2 => PixelFormat::RGBA4,
         3 => PixelFormat::RGBA8,
         4 => PixelFormat::DXT1,
         5 => PixelFormat::DXT1A,
         6 => PixelFormat::DXT3,
         7 => PixelFormat::DXT5,
         _ => PixelFormat::ALPHA8,
      }
   }
}
