//! Loading animated sprites from XML files

use crate::common::*;
use crate::error::*;
use crate::xml::*;

#[derive(Debug, PartialEq, Fragment, Clone)]
pub struct AnimatedPoly {
   pub frames: Vec<Frame>,
}

#[derive(Debug, PartialEq, Fragment, Clone)]
pub struct Frame {
   pub delay_ms: i32,
   #[xml(required)]
   pub poly: TexturedPoly,
}

#[derive(Debug, PartialEq, Fragment, Clone)]
pub struct TexturedPoly {
   #[xml(required)]
   pub hull: Hull,
   pub texture: Option<TransformedImage>,
}

impl TexturedPoly {
   pub fn has_texture(&self) -> bool {
      self.texture.is_some() && self.texture.as_ref().unwrap().image.path.trim() != ""
   }
}

#[derive(Debug, PartialEq, Fragment, Default, Clone)]
pub struct Image {
   #[xml(required)]
   pub path: String,
}

#[derive(Debug, PartialEq, Fragment, Default, Clone)]
pub struct TransformedImage {
   pub image: Image,
   pub xform: TransformComponents,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Hull {
   pub points: Vec<Vec2>,
   pub colors: Vec<Colour>,
}

impl Attribute for Hull {
   fn parse_attr(s: &str) -> Result<Self> {
      let (points, colours) = parse_hull(s)?;
      Ok(Hull {
         points,
         colors: colours,
      })
   }
   fn write_attr(&self) -> String {
      write_hull(&self.points, &self.colors)
   }
}

/// Parse a hull string into a list of points and colours
fn parse_hull(s: &str) -> Result<(Vec<Vec2>, Vec<Colour>)> {
   let mut color = DEFAULT_COLOR;
   let mut points = vec![];
   let mut colors = vec![];

   let x: Vec<&str> = s.split(',').collect();
   let mut i = 0;
   while i < x.len() {
      if x[i].is_empty() {
         break;
      }
      if x[i].starts_with('!') {
         color = Colour::parse_attr(&x[i][1..])?;
         i += 1;
      } else {
         if i + 1 == x.len() {
            bail!("Incomplete hull '{:?}'", s);
         }
         let point = Vec2 {
            x: x[i].parse()?,
            y: x[i + 1].parse()?,
         };
         points.push(point);
         colors.push(color);
         i += 2;
      }
   }
   Ok((points, colors))
}

/// Write a hull to a string.
fn write_hull(points: &[Vec2], colors: &[Colour]) -> String {
   let mut color = DEFAULT_COLOR;
   let mut hull = String::new();
   for (p_pos, p_col) in points.iter().zip(colors.iter()) {
      if *p_col != color {
         hull.push('!');
         hull.push_str(&p_col.write_attr());
         hull.push(',');
         color = *p_col;
      }

      let mut x = format!("{}", p_pos.x);
      if !x.contains('.') {
         x.push_str(".0");
      }
      let mut y = format!("{}", p_pos.y);
      if !y.contains('.') {
         y.push_str(".0");
      }
      hull.push_str(&format!("{},{},", x, y))
   }
   hull
}
